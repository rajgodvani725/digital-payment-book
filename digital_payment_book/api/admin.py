from django.contrib import admin
from .models import customer,PurchaseHistory,Cart,Product

admin.site.register(customer)
admin.site.register(PurchaseHistory)
admin.site.register(Cart)
admin.site.register(Product)
