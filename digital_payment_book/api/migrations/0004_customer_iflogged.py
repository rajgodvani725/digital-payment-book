# Generated by Django 2.2.18 on 2022-10-14 21:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_auto_20221014_2119'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='ifLogged',
            field=models.BooleanField(default=False),
        ),
    ]
