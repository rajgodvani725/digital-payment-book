from django.db import models
status=(('due','Due'),('completed','Completed'),('pending','Pending'))

class customer(models.Model):
    username = models.CharField(max_length=255, null=False)
    email = models.EmailField(max_length=255, null=False)
    phone_number = models.BigIntegerField(unique=True,null=True)
    first_name = models.CharField(max_length=255,default=False)
    last_name = models.CharField(max_length=255,default=False)
    password = models.CharField(max_length=50)
    ifLogged = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    token = models.CharField(max_length=500, null=True, default="")

    def __str__(self):
        return "{} : {}".format(self.username, self.email)

class Product(models.Model):
    id = models.AutoField(primary_key=True)
    name= models.CharField(max_length=255, null=False)
    wholesaleprice = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
    retail_price = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
    quantity = models.CharField(max_length=50)

    def __str__(self):
        return "{} : {}".format(self.name, self.retail_price)

class Cart(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.ForeignKey(customer,on_delete=models.CASCADE)
    total = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
    status  = models.CharField(max_length = 50,choices=status,null=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} : {}".format(self.username, self.status)

class PurchaseHistory(models.Model):
    customer = models.ForeignKey(customer,on_delete=models.CASCADE)
    product = models.ForeignKey(Product,on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()
    total_product_amount = models.IntegerField(null=True)
    cart_id = models.ForeignKey(Cart,on_delete=models.CASCADE)

    def __str__(self):
        return "{} : {}".format(self.customer, self.total_product_amount)

